from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import authenticate , login
# Create your views here.
from django.views import View
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, get_object_or_404

from analytics.models import ClickEvent

from .forms import SubmitUrlForm
from .models import SURL

from .validators import validate_url
# Create your views here.

def home_view_fbv(request, *args, **kwargs):
    if request.method == "POST":
        print(request.POST)
    return render(request, "shortener/home.html", {})


class HomeView(View):
    def get(self, request, *args, **kwargs):
        the_form = SubmitUrlForm()
        bg_image = 'https://upload.wikimedia.org/wikipedia/commons/0/05/20100726_Kalamitsi_Beach_Ionian_Sea_Lefkada_island_Greece.jpg'
        context = {
            "title": "SURL",
            "form": the_form,
            "bg_image": bg_image
        }
        return render(request, "shortener/home.html", context) # Try Django 1.8 & 1.9 http://joincfe.com/youtube

    def post(self, request, *args, **kwargs):
        form = SubmitUrlForm(request.POST)
        context = {
            "title": "surl.in",
            "form": form
        }
        template = "shortener/home.html"
        if form.is_valid():
            new_url = form.cleaned_data.get("url")
            # print(newurl)
            # new_url = validate_url(newurl)
            #if newurl
            print(new_url)
            print(request.user.is_authenticated)
            if request.user.is_authenticated:
                obj, created = SURL.objects.get_or_create(url=new_url,userid=request.user.id)
            else:
                obj, created = SURL.objects.get_or_create(url=new_url)

            context = {
                "object": obj,
                "created": created,
            }
            if created:
                template = "shortener/success.html"
            else:
                template = "shortener/already-exists.html"

        return render(request, template ,context)
#
# def URLRedirectView(request ,  shortcode=None, *args, **kwargs):
#
#
#     obj = SURL.objects.filter(shortcode__iexact=shortcode)
#
#     url = obj.first()
#     return HttpResponseRedirect(url.url)



class URLRedirectView(View):

    def get(self, request, shortcode=None, *args, **kwargs):
        print('here')
        qs = SURL.objects.filter(shortcode__iexact=shortcode)
        print(shortcode)
        if qs.count() != 1 and not qs.exists():
            raise Http404
        obj = qs.first()
        print(obj.url)
        print(ClickEvent.objects.create_event(obj))
        return HttpResponseRedirect(obj.url)
