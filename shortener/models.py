from django.db import models
from django.conf import settings
from django.utils.encoding import smart_text
from .utils import create_shortcode
from django.contrib.auth.models import User

SHORTCODE_MAX = getattr(settings, "SHORTCODE_MAX", 15)

class  SURL(models.Model):

    url         = models.CharField(max_length=220 )#, validators=[validate_url])
    shortcode   = models.CharField(max_length=15, unique=True, blank=True)
    userid     = models.CharField(max_length=5, default=0)
    updated     = models.DateTimeField(auto_now=True) #everytime the model is saved
    timestamp   = models.DateTimeField(auto_now_add=True) #when model was created
    active      = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        if self.shortcode is None or self.shortcode == "":
            self.shortcode = create_shortcode(self)
        if not "http" in self.url:
            self.url = "http://" + self.url
        super(SURL, self).save(*args, **kwargs)

    def get_short_url(self):
        return "http://surl.in/{shortcode}".format(shortcode=self.shortcode)
