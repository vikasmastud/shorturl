"""surl URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path , re_path
from user.views import  login_page , register_page , home_page , contact_page , TrackMg ,LogOut
#from django.contrib.auth.views import logout
from shortener.views import HomeView, URLRedirectView
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('tracurl',TrackMg),
    path('logout',LogOut ),#,{'next_page':settings.LOGOUT_REDIRECT_URL},name  = 'logout' ),
    path('login',login_page ),
    path('join',register_page ),
    path('contact',contact_page),
    path('', HomeView.as_view()),
    re_path('(?P<shortcode>[\w-]+)/$', URLRedirectView.as_view(), name='scode'),
]
