# Generated by Django 2.1.1 on 2018-09-21 19:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clickevent',
            name='s_url',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='shortener.SURL'),
        ),
    ]
