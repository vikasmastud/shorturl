from django import forms
from django.contrib.auth.models import User
from .util import validate_password_strength ,username_length

class LoginForm(forms.Form):
    username = forms.CharField(
            widget=forms.TextInput(
            attrs={
                "class":"form-control",
                "placeholder":"Name"
            }
        )
    )
    password = forms.CharField(
            widget=forms.PasswordInput(
            attrs={
                "class":"form-control",
                "placeholder":"Password"
            }
        )
    )


class RegisterForm(forms.Form):
    username=forms.CharField(
        widget=forms.TextInput(
            attrs={
                "class":"form-control",
                "placeholder":"NAME"
            }
        )
    )
    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={
                "class":"form-control",
                "placeholder":"Your Eamil"
            }
        )
    )
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                "class":"form-control",
                "placeholder":"password"
            }
        )
    )
    password2 = forms.CharField(label = "Confirm Password" ,widget=forms.PasswordInput(
                attrs={
                    "class":"form-control",
                    "placeholder":"re-enter Password"
                }
    ))

    def clean_email(self):
        email = self.cleaned_data.get('email')
        qs = User.objects.filter(email=email)
        print(qs.exists())
        print(email)
        if qs.exists():
            raise forms.ValidationError("email is taken")
        return email
    def clean_username(self):
        username = self.cleaned_data.get('username')
        qs = User.objects.filter(username=username)
        print(qs.exists())
        username_length(username)
        print(username)
        if qs.exists():
            raise forms.ValidationError("Username is taken")
        return username
    def clean(self):
        data = self.cleaned_data
        password = self.cleaned_data.get('password')
        validate_password_strength(password)
        password2 = self.cleaned_data.get('password2')
        print(password)
        print(" ")
        print(password2)
        if password != password2:
            raise forms.ValidationError("Passwords must match.")

        return data
    # def clean_email(self):
    #     email=self.cleaned_data.get("email")
    #     if not "gmail.com" in email:
    #         raise forms.ValidationError("Email has to be gmail.com")
    #
    #     return email

class ContactForm(forms.Form):
    username=forms.CharField(
        widget=forms.TextInput(
            attrs={
                "class":"form-control",
                "placeholder":"NAME"
            }
        )
    )
    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={
                "class":"form-control",
                "placeholder":"Your Eamil"
            }
        )
    )
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                "class":"form-control",
                "placeholder":"password"
            }
        )
    )

    password2 = forms.CharField(label = "Confirm Password" ,widget=forms.PasswordInput(
                attrs={
                    "class":"form-control",
                    "placeholder":"re-enter Password"
                }
    ))

    def clean_username(self):
        username = self.cleaned_data.get('username')
        qs = User.objects.filter(username=username)
        print(qs.exists())
        print(username)
        if qs.exists():
            raise forms.ValidationError("Username is taken")
        return username
    def clean_email(self):
        email = self.cleaned_data.get('email')
        qs = User.objects.filter(email=email)
        print(qs.exists())
        print(email)
        if qs.exists():
            raise forms.ValidationError("email is taken")
        return email

    def clean(self):
        data = self.cleaned_data
        password = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('password2')
        print(password)
        print(" ")
        print(password2)
        if password != password2:
            raise forms.ValidationError("Passwords must match.")
        return data
