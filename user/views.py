from django.http import HttpResponse,HttpResponseRedirect
from django.shortcuts import render , redirect
from .forms import  LoginForm , RegisterForm ,ContactForm
from django.contrib.auth import authenticate , login ,logout
from django.contrib.auth.models import User
from django.conf import settings

from shortener.forms import SubmitUrlForm
from shortener.models import SURL
def home_page(request):
    context = {
        "title":"Hello World",
        "content" : "Welcome to the home page",
        "premium_content" : "YEAHHHHHH"
    }
    return render(request,"home_page.html",context)

def contact_page(request):
    contact_form = ContactForm(request.POST or None)
    #print(contact_form)
    context = {
        "title": "Contact page",
        "form": contact_form
    }
    print(contact_form.is_valid())
    if contact_form.is_valid():
        print(contact_form.cleaned_data)
    # if request.method == "POST":
    #     print(request.POST)
    #
    #     print(request.POST.get("fullname"))
    #     print(request.POST.get("email"))

    return render(request,"contact/view.html",context)
def login_page(request):
    if request.user.is_authenticated:
        the_form = SubmitUrlForm()
        bg_image = 'https://upload.wikimedia.org/wikipedia/commons/0/05/20100726_Kalamitsi_Beach_Ionian_Sea_Lefkada_island_Greece.jpg'
        context = {
            "title": "SURL",
            "form": the_form,
            "bg_image": bg_image
        }
        return render(request, "shortener/home.html", context)
    form = LoginForm(request.POST or None)

    bg_image = "https://mdbootstrap.com/img/Photos/Horizontal/Nature/full%20page/img%2820%29.jpg"
    context = {
        "form":LoginForm(),
        "bg_image": bg_image
    }
    print(request.user.is_authenticated)
    if form.is_valid():
        print(form.cleaned_data)
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password")
        user = authenticate(request , username = username , password = password )
        print(request.user.is_authenticated)

        if user is not None:
            login(request , user)
            #context['form'] = form
            return redirect("/")
        else:
            print("Error")
    return render(request , "auth/login.html",context)

def register_page(request):
    if request.user.is_authenticated:
        the_form = SubmitUrlForm()
        bg_image = 'https://upload.wikimedia.org/wikipedia/commons/0/05/20100726_Kalamitsi_Beach_Ionian_Sea_Lefkada_island_Greece.jpg'
        context = {
            "title": "SURL",
            "form": the_form,
            "bg_image": bg_image
        }
        return render(request, "shortener/home.html", context)
    form = RegisterForm(request.POST or None)
    bg_image = "https://mdbootstrap.com/img/Photos/Horizontal/Nature/full%20page/img%2811%29.jpg"

    #print(contact_form)
    context = {
        "title": "Create your Personal account",
        "form": form,
        "bg_image": bg_image
    }

    if form.is_valid():
        print(form.cleaned_data)
        username = form.cleaned_data.get("username")
        email = form.cleaned_data.get("email")
        password = form.cleaned_data.get("password")
        user = User.objects.create_user(username, email , password)

    return render(request , "auth/register.html", context)
def LogOut(request):
        form = LoginForm(request.POST or None)

        bg_image = "https://mdbootstrap.com/img/Photos/Horizontal/Nature/full%20page/img%2820%29.jpg"
        context = {
            "form":LoginForm(),
            "bg_image": bg_image
        }
        logout(request)
        return render(request , "auth/login.html",context)
def TrackMg(request):
    bg_image = "https://mdbootstrap.com/img/Photos/Horizontal/Nature/full%20page/img%2811%29.jpg"
    context = {
        "title": "Create your Personal account",
        "bg_image": bg_image
    }
    if request.user.is_authenticated:
        obj = SURL.objects.filter(userid=request.user.id)
        for i in obj:
            print(i.clickevent_set.count)
        context['object'] = obj
        return render(request , "Explorer/Explorer.html", context)
    else:
        return render(request , "auth/login.html", context)
    return render(request , "Explorer/Explorer.html", context)
